﻿## For Instances
$servers = aws ec2 describe-instances --query "Reservations[*].Instances[*].InstanceId" --output text

foreach ($server in $servers) {
    $dpStatus = aws ec2 describe-instance-attribute --instance-id $server --attribute disableApiTermination --output text #>> C:\Users\kdsouza\Downloads\dpattribute.txt
    $instanceName = aws ec2 describe-instances --filter Name=instance-id,Values=$server --query "Reservations[*].Instances[*].{Name:Tags[?Key=='Name']|[0].Value}" --output text
    Write-Host ($instanceName+': '+ $dpStatus)
}


## For Load Balancers
$loadbalancers = aws elbv2 describe-load-balancers --query "LoadBalancers[*].[LoadBalancerArn]" --output text 

foreach ($loadbalancer in $loadbalancers){
    $tpStatus = aws elbv2 describe-load-balancer-attributes --load-balancer-arn $loadbalancer --query "Attributes[?Key == 'deletion_protection.enabled'].Value" --output text
    $elbName = aws elbv2 describe-load-balancers --load-balancer-arns $loadbalancer --query "LoadBalancers[*].[LoadBalancerName]" --output text 
    Write-Output ($elbName+': '+ $tpStatus)
}
